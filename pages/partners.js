import React from 'react';
import Head from 'next/head';
import { Box } from 'grommet';
import PartnerHeaderContainer from '@components/halfScreenContainers/PartnerHeaderContainer';
import SponsorContainer from '@components/halfScreenContainers/SponsorContainer';
import { LINKS } from 'helpers/globals';
import PARTNERS from 'helpers/partnerValues';

const Partners = () => {
  return (
    <>
      <Head>
        <title>Partners: Veteran Trash Talk</title>
      </Head>
      <Box fill="horizontal">
        <PartnerHeaderContainer />
        <Box pad="small">
          <SponsorContainer
            width={580}
            height={120}
            title="The Veteran's Mortgage Source Powered by Canopy Mortgage"
            info="Our mission is to educate as many Active Duty Military and Veterans as possible on the benefits of home ownership using their VA Home Loan"
            src="/VHM.webp"
            href={LINKS.CANOPY}
            showLightBg
          />
          <SponsorContainer
            width={580}
            height={150}
            title="PCS to Paradise"
            info="We're mission-ready to facilitate your family’s smooth PCS relocation to Hawaii."
            src="/PCS.webp"
            href={LINKS.PCS}
            showLightBg
          />
          <SponsorContainer
            title="The Ultimate Sacrifice Foundation"
            info={PARTNERS.SSS}
            src="/the-ultimate-sacrifice-logo.webp"
            href={LINKS.USF}
            width={520}
            height={73}
          />
          <SponsorContainer
            title="Ventura Training and Athletics"
            info={PARTNERS.VTA}
            src="/VTA.webp"
            href={LINKS.VTA}
          />
          <SponsorContainer
            title="10th Mountain Whisky & Spirit Company"
            info={PARTNERS.TENTH}
            src="/10mtn.webp"
            href={LINKS.TEN_MTN}
          />
          <SponsorContainer
            title="Tier-1 Pro Inspections"
            info={PARTNERS.TIER}
            src="/tier1.webp"
            href={LINKS.TIER1}
            showLightBg
          />
          <SponsorContainer
            title="Project Roll Call"
            info={PARTNERS.PROJECT_ROLL_CALL}
            src="/ProjectRollCall.webp"
            href={LINKS.PROJECT_ROLL_CALL}
            width={200}
            height={100}
          />
          <SponsorContainer
            title="Help Me PTSD"
            info={PARTNERS.HELP_ME_PTSD}
            src="/HelpPtsd.webp"
            href={LINKS.HELP_ME_PTSD}
            showLightBg
          />
          <SponsorContainer
            title="AZ Metro News"
            info={PARTNERS.AZ_METRO_NEWS}
            src="/Azmetro.webp"
            href={LINKS.AZ_METRO}
            showLightBg
          />

          <SponsorContainer
            title="Stop Soldier Suicide"
            info={PARTNERS.SSS}
            src="/bigSSS.webp"
            href={LINKS.SSS}
            width={520}
            height={73}
          />
        </Box>
      </Box>
    </>
  );
};

export default Partners;
