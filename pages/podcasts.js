import PodcastsHeaderContainer from '@components/halfScreenContainers/PodcastsHeaderContainer';
import AwardWinningPodcast from '@components/podcastsPageLayout/AwardWinningPodcast';
import PodcastContainer from '@components/podcastsPageLayout/PodcastContainer';
import { vttOrange } from '@styles/theme';
import { Box, Heading, Text } from 'grommet';
import { LINKS } from 'helpers/globals';
import Head from 'next/head';

const fmwAbout =
  'Just when you thought the fellas were having all the fun, meet Len and Leah who have had their fair share of days when they just wanted to give someone a good ole throat punch. We will talk about the good, bad, and the ugly when it comes to service, life, all it entails, tackling specific issues that women veterans face.';

const stonedAbout =
  "The goal of THE STONED VET is to build the community around the VET!! Let's build the camaraderie that we lost when we left the military!!!! SFMF!!!!' This marine vet tackles today's issues and current events with a laid back sense of humor and relaxed vibe.";

const sportsBetting =
  'A weekly youtube show where the gang picks UFC fights and NFL games. This is nothing but trash talk and sports talk!';

const fithPrinciple =
  "The 5th Principle of Patrolling is 'Common-Sense.' In this show, a Ranger qualified infantryman and a Special Forces soldier provide their take on today's issues using a common-sense approach. They provide humorous and unique views about the world today and share how you can become an expert in the '5th Principle.'";

const mhwAbout =
  'A podcast by Clifford Bauman, a 34 year Army veteran, suicide survivor, and an internationally known motivational speaker. His goal is to help reduce the stigma of asking for help.';

export default function Podcasts() {
  return (
    <>
      <Head>
        <title>Podcasts and shows</title>
      </Head>
      <Box fill="horizontal">
        <PodcastsHeaderContainer />

        <Box fill="horizontal" align="center">
          <Heading level="2" color={vttOrange} margin={{ bottom: 'none' }}>
            Our Current Line Up
          </Heading>
          <Text margin="medium" textAlign="center">
            We take great pride in every Podcast that carries the VTT Brand. Each podcast is unique
            but shares the common goals of building the veteran community, providing resources for
            soldiers and veterans, and promoting veteran businesses.
          </Text>
          <AwardWinningPodcast />
          <PodcastContainer
            src="/spp.webp"
            title="SPORTS BETTING PICKS"
            about={sportsBetting}
            href={LINKS.YOUTUBE}
          />
          <PodcastContainer
            src="/mhw.webp"
            title="The Mental Health Warrior Podcast"
            about={mhwAbout}
            href={LINKS.MHW}
          />
          <PodcastContainer
            src="/5thPrinc.jpg"
            title="THE 5th PRINCIPLE OF PATROLLING"
            about={fithPrinciple}
            href={LINKS.YOUTUBE}
          />
          <PodcastContainer
            src="/tpm.webp"
            title="THROAT PUNCH MONDAY"
            about={fmwAbout}
            href={LINKS.TPM}
          />
          <PodcastContainer
            src="/stonedVet.webp"
            title="THE STONED VET"
            about={stonedAbout}
            href={LINKS.STONED_VET_YOUTUBE}
          />
        </Box>
      </Box>
    </>
  );
}
