import { ABOUT_US } from 'helpers/globals';

const eeClient = require('elasticemail-webapiclient').client;

export default async function (req, res) {
  const options = {
    apiKey: process.env.ELASTIC_EMAIL_API_KEY,
    apiUri: 'https://api.elasticemail.com/',
    apiVersion: 'v2'
  };

  const EE = new eeClient(options);

  const { email, name, message } = req.body;

  const emailParams = {
    subject: 'Mail from VeteranTrashTalk.com',
    to: ABOUT_US.EMAIL,
    from: email,
    replyTo: ABOUT_US.EMAIL,
    body: message,
    fromName: name,
    bodyType: 'Plain'
  };

  try {
    await EE.Account.Load();
    await EE.Email.Send(emailParams);
    res.status(200).send('message sent successfully');
  } catch (error) {
    console.log(error);
    res.status(500).send('there was a problem sending your message');
  }
}
