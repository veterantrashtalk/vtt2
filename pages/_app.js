import React from 'react';
import PropTypes from 'prop-types';
import HeaderBar from '../components/headerBar/HeaderBar';
import '../styles/styles.css';
import { Grommet, ResponsiveContext } from 'grommet';
import FooterBar from '../components/footerBar/FooterBar';
import theme from '../styles/theme';

function MyApp({ Component, pageProps }) {
  const [darkMode, setDarkMode] = React.useState(false);

  const toggle = () => {
    setDarkMode(!darkMode);
  };

  return (
    <Grommet theme={theme} full themeMode={'dark'}>
      <ResponsiveContext.Consumer>
        {(size) => (
          <>
            <HeaderBar size={size} toggleDarkMode={toggle} darkMode={darkMode} />
            <Component {...pageProps} size={size} />
            <FooterBar />
          </>
        )}
      </ResponsiveContext.Consumer>
    </Grommet>
  );
}

export default MyApp;

MyApp.propTypes = {
  Component: PropTypes.elementType.isRequired,
  pageProps: PropTypes.object.isRequired
};
