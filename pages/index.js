import React from 'react';
import { Box, ResponsiveContext } from 'grommet';
import Head from 'next/head';
import CallToAction from '../components/callToAction/CallToAction';
import ConnectBar from '../components/connectBar/ConnectBar';
import HeaderOneContainer from '../components/halfScreenContainers/HeaderOneContainer';
import SponsorContainer from '@components/halfScreenContainers/SponsorContainer';
import { LINKS } from '../helpers/globals';
import VttHourLayout from '../components/homePageLayout/VttHourLayout/VttHourLayout';
import MeetTheHosts from '../components/homePageLayout/MeetTheHosts/MeetTheHosts';
import Community from '../components/shared/Community';
import WhatPeopleAreSaying from '@components/whatPeopleAreSaying/WhatPeopleAreSaying';
import Award from '@components/shared/Award';

export default function Home() {
  const size = React.useContext(ResponsiveContext);
  return (
    <>
      <Head>
        <title>Veteran Trash Talk</title>
      </Head>
      <Box fill="horizontal">
        <HeaderOneContainer />
        <Box direction="column" justify="center" margin={{ top: '1em' }}>
          <CallToAction size={size} />
          <VttHourLayout />
          <Box
            direction="row"
            justify="center"
            margin={size === 'large' ? { bottom: '1em' } : { left: '.5em', bottom: '1em' }}>
            <Award />
          </Box>
          <MeetTheHosts />
          <ConnectBar />
          <SponsorContainer
            featured
            width={580}
            height={120}
            title="The Veteran's Mortgage Source Powered by Canopy Mortgage"
            info="Our mission is to educate as many Active Duty Military and Veterans as possible on the benefits of home ownership using their VA Home Loan"
            src="/VHM.webp"
            href={LINKS.CANOPY}
            showLightBg
          />
          <SponsorContainer
            featured
            width={580}
            height={150}
            title="PCS to Paradise"
            info="We're mission-ready to facilitate your family’s smooth PCS relocation to Hawaii."
            src="/PCS.webp"
            href={LINKS.PCS}
            showLightBg
          />
          <Community />
          <WhatPeopleAreSaying />
        </Box>
      </Box>
    </>
  );
}
