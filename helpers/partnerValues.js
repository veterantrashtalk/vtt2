import React from 'react';
import { Box } from 'grommet';

const VTA =
  'We offer a unique approach to strength training in combination with recovery and restoration techniques. We provide personalized, one-on-one service at a private and intimate studio the Ventura California. Our approach is straightforward. Our clients achieve their goals by tackling challenging, tailor-made workouts that are results-oriented, do not require an unrealistic time commitment, and are dependably safe';
const SSS =
  'Not all crisis looks the same. Stop Soldier Suicide is our promise to those who are facing it. Our struggles may be loud or silent; all-consuming or passing; during service or any time after; a moment or a lifetime.';

const ZACH = (
  <Box direction="column" align="center" justify="center">
    <span>
      <em>Senior Loan Officer</em>
    </span>
    <span>C 8088610586</span>
    <span>P 8084668531</span>
    <span>F 8084668531</span>
    <span>NMLS: 1727603</span>
    <span>
      Are you looking for a team you can trust for your mortgage needs? The Farkas Group is/are our
      trusted partners on the podcast. Their goal is to educate Veterans across the country on their
      earned VA Home Loan Benefit. If you’re looking to purchase or refinance your home, reach out
      to these guys for competitive rates or any information you may need on obtaining a VA mortgage
      for your property.
    </span>
  </Box>
);

const TENTH =
  'The 10th Mountain Whiskey & Spirit Company is Vail, Colorado’s premier distillery, crafting spirits from locally sourced ingredients in the name of the mountain lifestyle that was influenced by the original 10th Mountain soldiers.';

const TIER =
  'At Tier-1 Pro Inspections we provide the highest quality home inspections at the best value for your money. We are committed to building our customer’s confidence in their home buying decision with an unbiased, independent and practical assessment of the overall condition of their home.';

const PROJECT_ROLL_CALL =
  'Our mission at “Project Roll Call” is two-fold: to raise awareness on all veterans issues and to link both our veterans and their families with pertinent, reliable, and proven resources.';

const HELP_ME_PTSD =
  'Help Me PTSD is designed to provide continual services for trauma survivors and their family members. We understand how devastating  life’s unexpected tragedies can be. These events can impact both survivor and their entire social support system to include family. We provide resources that help people heal. These resources will help any willing person overcome their past by focusing on their own personal healing journey. Our programs encompass the body, mind, and spiritual healing process. From Counseling and Professional coaching, to Workshops and Seminars. Our programs are the future of mental wellness!';

const AZ_METRO_NEWS =
  'Free News and Video Content! While many other media and news companies charge consumers for content, we believe that the news and content should be free! Support us by creating a membership and publsihing amazing content if you choose! Enjoy News, Local and national stories on our growing platform.';

const BARRAGE =
  'Barrage  is an award-winning veteran owned and operated gourmet coffee company.  Barrage not only has quality coffee; but a CBD infused line to help you start your day. Part of the proceeds go to Battle on the HomeFront and other Veteran out-reach programs. Our goal is to give back to veterans in every way possible.';

const ELEVATED =
  'Elevated Hemp Solutions is the parent company of Happy Place Hemp and Elevated Soul CBD brands. We’re a farm-to-shelf hemp company located in Wisconsin. We produce and distribute full-spectrum CBD products.';

const CANOPY = "We think getting a home loan shouldn't be intimidating or overwhelming. That's why we're focusing on the benefits that matter most to our clients."
const PARTNERS = {
  ELEVATED,
  VTA,
  ZACH,
  TIER,
  SSS,
  TENTH,
  PROJECT_ROLL_CALL,
  HELP_ME_PTSD,
  AZ_METRO_NEWS,
  BARRAGE,
  CANOPY
};

export default PARTNERS;
