const FACEBOOK_GROUP = 'https://www.facebook.com/groups/204859031456568';

const FACEBOOK_PAGE = 'https://www.facebook.com/veterantrashtalk';

const INSTAGRAM = 'https://www.instagram.com/veterantrashtalk/';

const YOUTUBE = 'https://www.youtube.com/channel/UCXsePtYQy6e-z0Th1pboSvg';

const SPOTIFY = 'https://open.spotify.com/show/2wqhvL6emsmbqoInLh8pHr?si=PNZn7mGgSo-vz-guBvalRg';

const APPLE = 'https://podcasts.apple.com/us/podcast/veteran-trash-talk-hour/id1517337049';

const TWITTER = 'https://twitter.com/veteran_talk';

const SHOP = 'https://shop.veterantrashtalk.com/';

const SSS = 'https://stopsoldiersuicide.org/get-help';

const USF = 'https://ultimatesacrificefoundation.org/';

const VTA = 'https://www.facebook.com/Venturatrainingandathletics';

const TEN_MTN = 'https://10thwhiskey.com/';

const TIER1 = 'https://tier1proinspections.com/';

const STONED_VET_YOUTUBE = 'https://www.youtube.com/channel/UCJk55bmjx03NpyWSOyCbBGw';

const PROJECT_ROLL_CALL = 'http://projectrollcall.org';

const AZ_METRO = 'https://azmetronews.com/';

const HELP_ME_PTSD = 'https://helpmeptsd.org/';

const BARRAGE = 'https://barragecoffee.com/';

const BTB = 'https://www.youtube.com/watch?v=bsHhVq3AYBE&list=PLbzRBX7A6FdAHPakgAMldsykqdPBGk3uT';

const TPM = 'https://www.youtube.com/watch?v=6UCQ6uRSvBs&list=PLbzRBX7A6FdAR0VbEHqu16TTlzUh6bNOH';

const EHL = 'https://shop.elevatedhempsolutions.com';

const EHB = 'https://shop.elevatedhempsolutions.com/happy-place-hemp/';

const MHW = 'https://www.youtube.com/watch?v=sE15ymSYpj0&list=PLbzRBX7A6FdBhBwaQKeECqjLkQNTxDOAS';

const CANOPY = 'https://veteransmortgagesource.com/';
const PCS = 'https://pcstoparadise.org/';

export const LINKS = {
  CANOPY,
  USF,
  EHL,
  EHB,
  FACEBOOK_GROUP,
  FACEBOOK_PAGE,
  INSTAGRAM,
  YOUTUBE,
  SPOTIFY,
  APPLE,
  TWITTER,
  SHOP,
  SSS,
  VTA,
  TEN_MTN,
  TIER1,
  STONED_VET_YOUTUBE,
  PROJECT_ROLL_CALL,
  AZ_METRO,
  HELP_ME_PTSD,
  TPM,
  BTB,
  BARRAGE,
  MHW,
  PCS
};

//TODO fix errors

const NICK_EMAIL = 'bigearl@veterantrashtalk.com';
const JOE_EMAIL = 'triggeredjoe@veterantrashtalk.com';
const DAVE_EMAIL = 'faceman@veterantrashtalk.com';

export const ABOUT_US = { NICK_EMAIL, JOE_EMAIL, DAVE_EMAIL };
