require('dotenv').config();
module.exports = {
  env: {
    ELASTIC_EMAIL_API_KEY: process.env.ELASTIC_EMAIL_API_KEY,
    GET_RESPONSE_API_KEY: process.env.GET_RESPONSE_API_KEY,
    GET_RESPONSE_CAMPAIGN_ID: process.env.GET_RESPONSE_CAMPAIGN_ID
  }
};
