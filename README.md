This is the main website for Veteran Trash Talk.

This website  is built with blood sweat and [Next.js](https://nextjs.org/). 

## Quickstart
1. Make sure NPM and Node is installed on your machine. If you don't know how, here are a couple of resources: [WINDOWS](https://phoenixnap.com/kb/install-node-js-npm-on-windows), [MAC](https://treehouse.github.io/installation-guides/mac/node-mac.html), LINUX (https://linuxconfig.org/install-npm-on-linux)

2. It helps to have a good editor. We recommend [VS Code](https://code.visualstudio.com/). 

3. Open up a terminal and Clone the repository. If you are on windows we recommend using Bash, for further guidance on that see [here](https://itsfoss.com/install-bash-on-windows/).

4. Open a terminal and clone the repository.

5. Change to the directory of the newly installed repo ```cd vtt```

6. Run the command: ```npm install```

7. Run the command: ```npm run dev```

8. This will open the port 3000 in the browser. If your browser doesn't open automatically, open up a browser and type ```localhost:3000``` as the URL. 


## Run tests
1. In the VTT2 directory, run the command ```npm run test```


## Submit an issue
If you want to submit an issue or a bug, please do so with as much detail as possible. If it is a bug, please list the steps you can use to recreate the bug. Screenshots are welcome.

## Make a Merge Request
If you have an improvement or a fix for an issue, please submit a Merge Request.

