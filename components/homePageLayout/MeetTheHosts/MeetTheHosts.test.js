import React from 'react';
import { render } from '@testing-library/react';
import MeetTheHosts from './MeetTheHosts';

describe('MeetTheHosts tests', () => {
  it('should render', () => {
    const { container } = render(<MeetTheHosts />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
