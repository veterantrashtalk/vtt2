import React from 'react';
import Link from 'next/link';
import { Box, Button, Heading, Paragraph, ResponsiveContext } from 'grommet';
import AvatarWIthText from '../../shared/AvatarWIthText';

const MeetTheHosts = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <>
      <Box justify="center" align="center" margin={{ top: '2em' }}>
        <Heading level="2" margin="none">
          Meet The Hosts
        </Heading>
      </Box>
      <Box
        direction={size === 'small' ? 'column' : 'row'}
        justify="center"
        align="center"
        margin="none">
        <AvatarWIthText src="/NickC.webp" fallback="/NickC.jpg" size="large" name="Nick Cottrill" />
        <AvatarWIthText
          src="/DavidT.webp"
          fallback="/DavidT.jpg"
          size="large"
          name="David Trentin"
        />
        <AvatarWIthText src="/JoeB.webp" fallback="/JoeB.jpg" size="large" name="Joe Bridson" />
        <AvatarWIthText
          src="/BuddyB.webp"
          fallback="/BuddyB.jpg"
          size="large"
          name="Buddy Beckwith"
        />
        <Box direction="column" align="center" margin="medium">
          <Paragraph margin="small" textAlign={size === 'small' ? 'center' : 'start'} flex>
            The hosts of Veteran Trash Talk Hour served together in C co, 2-505 PIR, 82nd Airborne
            Division. They enjoy trash talking each other and playing beer pong. Once a week, they
            get it together enough to do a podcast that is somewhat coherent.
          </Paragraph>
          <Link href="/about">
            <Button primary label="Find Out More" alignSelf="center" />
          </Link>
        </Box>
      </Box>
    </>
  );
};

export default MeetTheHosts;
