import React from 'react';
import { render } from '@testing-library/react';
import VttHourLayout from './VttHourLayout';

describe('VtHourLayout tests', () => {
  it('should render', () => {
    const { container } = render(<VttHourLayout />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
