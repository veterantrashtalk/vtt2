import React from 'react';
import { Header } from 'grommet';

const AppBar = (props) => (
  <Header
    direction="row"
    align="center"
    justify="between"
    background={{ dark: 'dark-1', light: 'light-1' }}
    pad={{ left: 'medium', right: 'small', vertical: 'small' }}
    elevation="medium"
    style={{ zIndex: '1' }}
    {...props}
  />
);

export default AppBar;
