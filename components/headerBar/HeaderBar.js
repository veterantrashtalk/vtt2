import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import AppBar from './AppBar';
import { Box, Button, Heading, Layer } from 'grommet';
import { FormClose, Menu } from 'grommet-icons';
import SelectedLink from '../shared/SelectedLink';
import { LINKS } from '../../helpers/globals';

export const StyledHeading = styled(Heading)`
  font-family: 'Unica One', sans-serif;
`;

const HeaderBar = (props) => {
  const { size, darkMode } = props;
  const [showMenu, setShowMenu] = React.useState(false);
  return (
    <>
      <AppBar darkMode={darkMode}>
        <StyledHeading level="3" margin="none">
          VETERAN TRASH TALK
        </StyledHeading>
        {size !== 'small' ? (
          <Box direction="row">
            <Box margin="small" />
            <SelectedLink href="/" name="home" />
            <SelectedLink href="/partners" name="partners" />
            <SelectedLink href="/about" name="about" />
            <SelectedLink href="/podcasts" name="podcasts" />
            <SelectedLink href={LINKS.SHOP} name="shop" />
          </Box>
        ) : (
          <Button
            id="main-menu-button"
            icon={<Menu />}
            onClick={() => {
              setShowMenu(true);
            }}
          />
        )}
      </AppBar>
      {showMenu && size == 'small' && (
        <Layer id="side-menu">
          <Box background="dark-1" tag="header" justify="end" align="center" direction="row">
            <Button
              id="close-menu-button"
              icon={<FormClose />}
              onClick={() => setShowMenu(false)}
            />
          </Box>
          <Box fill background="dark-1" align="center" justify="start" direction="column">
            <SelectedLink href="/" name="home" changeState={() => setShowMenu(false)} />
            <SelectedLink href="/partners" name="partners" changeState={() => setShowMenu(false)} />
            <SelectedLink href="/about" name="about" changeState={() => setShowMenu(false)} />
            <SelectedLink href="/podcasts" name="podcasts" changeState={() => setShowMenu(false)} />
            <SelectedLink href={LINKS.SHOP} name="shop" />
          </Box>
        </Layer>
      )}
    </>
  );
};

HeaderBar.propTypes = {
  size: PropTypes.string,
  toggleDarkMode: PropTypes.func,
  darkMode: PropTypes.bool
};

export default HeaderBar;
