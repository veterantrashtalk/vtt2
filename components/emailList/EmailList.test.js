import React from 'react';
import { render } from '@testing-library/react';
import EmailList from './EmailList';

describe('EmailList tests', () => {
  it('should render', () => {
    const { container } = render(<EmailList />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
