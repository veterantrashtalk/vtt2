import React from 'react';
import EmailListForm from './EmailListForm';
import { Box, ResponsiveContext, Text } from 'grommet';
import BoxWithBGImage from '@components/shared/BoxWithBGImage';

const EmailList = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <BoxWithBGImage
      url="/gray-helicopter.webp"
      gradient
      height="medium"
      pad={{ top: '1em', bottom: '1em' }}
      align="center"
      justify="around"
      direction={size === 'small' ? 'column' : 'row'}>
      <Box
        direction="column"
        justify="center"
        alignContent="center"
        pad="small"
        width={size !== 'small' ? { max: '50%' } : 'large'}>
        <Text size="large" weight="bold" textAlign="center">
          Join our newsletter.
        </Text>
        <Text size="large" weight="bold" textAlign="center">
          Get access to store discounts and learn about what is new with VTT.
        </Text>
      </Box>
      <EmailListForm />
    </BoxWithBGImage>
  );
};

export default EmailList;
