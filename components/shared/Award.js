import { Box, Heading } from 'grommet';
import Image from 'next/image';

const Award = () => {
  return (
    <Box direction="row" margin={{ bottom: '1em' }}>
      <Image src={'/awards.webp'} width={100} height={100} />
      <Heading level="4" textAlign="start" margin={{ left: '1em' }}>
        Voted 2021 Veteran Podcast Awards
        <br /> Best Army Podcast
      </Heading>
    </Box>
  );
};

export default Award;
