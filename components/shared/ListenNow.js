import React from 'react';
import { Box, ResponsiveContext, Text } from 'grommet';
import { LINKS } from 'helpers/globals';
import ClickableCard from '@components/callToAction/ClickableCard';
import { Spotify, Youtube } from 'grommet-icons';
import { vttOrange } from '@styles/theme';

const ListenNow = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <Box direction="column" justify="center" align="center" margin="medium">
      <Text size="xlarge" weight="bold">
        WATCH OR LISTEN NOW!
      </Text>
      <Text size="medium" margin={{ top: '1em' }}>
        VETERAN TRASH TALK HOUR
      </Text>

      <Box margin="small">
        <Text size="small" textAlign="center">
          Get the best military banter and rants! Listen to veteran stories. Learn about veteran
          businesses!
        </Text>
        <Box direction={size == 'small' ? 'column' : 'row'} align="center">
          <ClickableCard
            linkTo={LINKS.YOUTUBE}
            icon={<Youtube size="xlarge" color={vttOrange} />}
            text="Watch"
            level="4"
          />
          <ClickableCard
            linkTo={LINKS.SPOTIFY}
            icon={<Spotify size="xlarge" color={vttOrange} />}
            text="listen"
            level="4"
          />
        </Box>
      </Box>
    </Box>
  );
};

export default ListenNow;
