import React from 'react';
import { render } from '@testing-library/react';
import AvitarWithText from './AvatarWIthText';

describe('avatar with text', () => {
  it('should render', () => {
    const props = { size: 'small', src: '/test.jpg', fallback: '/test.png', name: 'test' };
    const { container } = render(<AvitarWithText {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
