import React from 'react';
import { render } from '@testing-library/react';
import LinkButton from './LinkButton';

jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: ''
    };
  }
}));

describe('link button component', () => {
  it('should render', () => {
    const props = { href: '/', label: 'Push Me' };
    const { container } = render(<LinkButton {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
