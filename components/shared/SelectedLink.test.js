import React from 'react';
import { render } from '@testing-library/react';
import SelectedLink from './SelectedLink';
jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '/',
      query: '',
      asPath: ''
    };
  }
}));
describe('selected link  component', () => {
  it('should render', () => {
    const props = { name: 'home', href: '/', changeState: jest.fn() };
    const { container } = render(<SelectedLink {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
