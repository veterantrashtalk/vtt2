import React from 'react';
import PropTypes from 'prop-types';
import { Box, Button, Text } from 'grommet';
import { Spinner } from './StyledHelpers';

const FormSubmitButton = ({ status }) => {
  const renderButtonLabel = () =>
    status.submitting ? <Spinner /> : status.submitted ? 'Submitted' : 'Submit';

  return (
    <Box direction="row" gap="medium">
      <Button
        type="submit"
        primary
        label={renderButtonLabel()}
        disabled={status.submitting || status.submitted}
      />
      {status.info.error && (
        <Box width="small">
          <Text size="xsmall" truncate wordBreak="break-all">
            {status.info.msg}
          </Text>
        </Box>
      )}
      {!status.info.error && status.submitted && (
        <Box width="small">
          <Text size="xsmall" truncate wordBreak="break-all">
            {status.info.msg}
          </Text>
        </Box>
      )}
    </Box>
  );
};

FormSubmitButton.propTypes = {
  status: PropTypes.shape({
    submitted: PropTypes.bool,
    submitting: PropTypes.bool,
    info: PropTypes.shape({ error: PropTypes.bool, msg: PropTypes.string })
  }).isRequired
};

export default FormSubmitButton;
