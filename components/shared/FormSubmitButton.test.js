import React from 'react';
import { render } from '@testing-library/react';
import FormSubmitButton from './FormSubmitButton';

describe('FormSubmitButton tests', () => {
  it('should render', () => {
    const status = { submitting: false, submitted: false, info: { error: false, msg: null } };
    const { container } = render(<FormSubmitButton status={status} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
