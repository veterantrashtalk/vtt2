import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { vttOrange } from '../../styles/theme';

export const StyledLink = styled.a`
  text-decoration: none;
  color: inherit;
  text-transform: uppercase;
  font-size: 14px;
  padding: 0.5em;
  margin: 0;
  border-bottom: ${(props) => (props.selected ? `2px solid ${vttOrange}` : '')};
`;

const SelectedLink = ({ href, name, changeState, hideSelection = false }) => {
  const router = useRouter();
  let selected = false;

  if (router.pathname === href && !hideSelection) {
    selected = true;
  }

  return (
    <Link href={href} passHref>
      <StyledLink data-testid="styledLink" onClick={changeState} selected={selected}>
        {name}
      </StyledLink>
    </Link>
  );
};

SelectedLink.propTypes = {
  href: PropTypes.string.isRequired,
  name: PropTypes.node.isRequired,
  changeState: PropTypes.func,
  hideSelection: PropTypes.bool
};

export default SelectedLink;
