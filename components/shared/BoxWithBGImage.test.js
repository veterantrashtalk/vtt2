import React from 'react';
import { render } from '@testing-library/react';
import BoxWithBGImage from './BoxWithBGImage';

describe('BoxWithBGImage tests', () => {
  it('should render', () => {
    const props = {
      gradient: true,
      url: 'test'
    };
    const { container } = render(<BoxWithBGImage {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
