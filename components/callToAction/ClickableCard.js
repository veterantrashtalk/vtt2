import React from 'react';
import PropTypes from 'prop-types';
import { Card, CardBody, CardFooter, Heading } from 'grommet';

const ClickableCard = ({ linkTo, icon, text, level = '2' }) => {
  return (
    <Card
      data_testid="card"
      onClick={() => window.open(linkTo)}
      elevation="none"
      round="1px"
      margin="medium"
      flex
      align="center">
      <CardBody pad="small">{icon}</CardBody>

      <CardFooter pad={{ horizontal: 'small' }} justify="center">
        <Heading level={level}>{text.toUpperCase()}</Heading>
      </CardFooter>
    </Card>
  );
};

ClickableCard.propTypes = {
  linkTo: PropTypes.string,
  text: PropTypes.string,
  icon: PropTypes.element,
  level: PropTypes.string
};

export default ClickableCard;
