import React from 'react';
import { render } from '@testing-library/react';
import CallToAction from './CallToAction';
jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: ''
    };
  }
}));

describe('call to action  component', () => {
  test('should render', () => {
    const { container } = render(<CallToAction />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
