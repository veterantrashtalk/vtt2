import React from 'react';
import { render } from '@testing-library/react';
import { Spotify } from 'grommet-icons';
import ClickableCard from './ClickableCard';
global.open = jest.fn();
describe('ClickableCard tests', () => {
  test('should render', () => {
    const props = { linkTo: 'test.com', icon: <Spotify />, text: 'test' };
    const { container } = render(<ClickableCard {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
