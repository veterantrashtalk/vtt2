import React from 'react';
import PropTypes from 'prop-types';
import { Box, ResponsiveContext, Text } from 'grommet';
import AvatarWIthText from '@components/shared/AvatarWIthText';

const TeamMember = ({ imageSize, src, fallback, name, aboutText }) => {
  const size = React.useContext(ResponsiveContext);

  return (
    <Box
      pad="small"
      direction={size === 'small' ? 'column' : 'row'}
      justify="center"
      align="center">
      <Box width="medium">
        <AvatarWIthText src={src} fallback={fallback} size={imageSize} name={name} />
      </Box>
      <Box width="large">
        <Text size="small" textAlign={size === 'small' ? 'center' : 'start'}>
          {aboutText}
        </Text>
      </Box>
    </Box>
  );
};

TeamMember.propTypes = {
  imageSize: PropTypes.string.isRequired,
  fallback: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  aboutText: PropTypes.string.isRequired
};

export default TeamMember;
