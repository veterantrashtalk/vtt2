import React from 'react';
import { render } from '@testing-library/react';
import MeetTheTeam from './MeetTheTeam';

describe('MeetTheTeam tests', () => {
  test('should render', () => {
    const { container } = render(<MeetTheTeam />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
