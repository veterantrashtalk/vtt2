import React from 'react';
import { Box, Heading, ResponsiveContext, Text } from 'grommet';
import { vttOrange } from '@styles/theme';

const Mission = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <Box direction={size === 'small' ? 'column' : 'row'}>
      <Box direction="column" align="center" justify="center" pad="small" margin="small">
        <Heading level="2" color={vttOrange}>
          Our Goals and Vision
        </Heading>
        <Text align="center">
          Everyone involved with Veteran Trash Talk, LLC is committed to preventing veteran suicide
          by offering a network of resources and support in a community of shared understanding. Our
          main goal is to have fun, promote veteran-owned businesses, and help veterans who are out
          of work. Everyone is invited to help understand the veteran culture and become an active
          part of the solution.
        </Text>
      </Box>
      <Box direction="column" align="center" justify="center" pad="small" margin="small">
        <Heading level="2" color={vttOrange}>
          Who We Are
        </Heading>
        <Text align="center">
          Veteran Trash Talk, LLC is group of smart and sophisticated veterans who will always
          pretend to know more than you. We are here to talk trash and support each other in a more
          proactive way than ever. We are a for-profit apparel company and future media &
          advertising global icon! VTT chose to operate as a for-profit company so we can provide
          for our families and help as many veterans as possible at the same time. We are here to
          help veterans get the beast inside of them out so we can crush it.
        </Text>
      </Box>
      <Box direction="column" align="center" justify="center" pad="small" margin="small">
        <Heading level="2" color={vttOrange}>
          Our Story
        </Heading>
        <Text align="center">
          The three founders met at Fort Bragg, NC in 2006 and have been brothers ever since. Joe
          and Nick talked about starting a venue to talk trash about any topic for years, but we
          needed a better reason to do it (other than having fun). We added Dave to the mix for his
          ravishing good looks and commitment. Our CFO is a nerdy friend of Joe’s who is committed
          to serving those who have served.
        </Text>
      </Box>
    </Box>
  );
};

export default Mission;
