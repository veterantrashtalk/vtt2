import React from 'react';
import { render } from '@testing-library/react';
import BannerContainer from './BannerContainer';

describe('BannerContainer tests', () => {
  test('renders without crashing', () => {
    const props = {
      href: 'test.com',
      src: '/image',
      color: '#fff',
      gradient: `linear-gradient(
        90deg,
        rgba(38, 69, 154, 1) 19%,
        rgba(35, 35, 98, 1) 40%,
        rgba(15, 21, 63, 1) 80%
      );`
    };
    const { container } = render(<BannerContainer {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
