import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Box } from 'grommet';

const ColoredBox = styled(Box)`
  background: ${(props) => `${props.color}`};
  background: ${(props) => `${props.gradient}`};
  div:first-of-type {
    display: block !important;
  }
`;

const ColoredBoxBanner = ({ ...rest }) => {
  return <ColoredBox {...rest} />;
};

ColoredBoxBanner.propTypes = {
  color: PropTypes.string.isRequired
};

export default ColoredBoxBanner;
