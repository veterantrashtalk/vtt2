import React from 'react';
import { render } from '@testing-library/react';
import ConnectBar from './ConnectBar';
jest.mock('next/router', () => ({
  useRouter() {
    return {
      route: '/',
      pathname: '',
      query: '',
      asPath: ''
    };
  }
}));

describe('connect bar component', () => {
  it('should render', () => {
    const { container } = render(<ConnectBar />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
