import React from 'react';
import { Heading, ResponsiveContext, Text } from 'grommet';
import BoxWithBGImage from '@components/shared/BoxWithBGImage';
import { vttOrange } from '@styles/theme';
import styled from 'styled-components';

const HeadingText = styled(Heading)`
  font-weight: 700;
  font-size: ${(props) => (props.size === 'small' ? `60px` : '8vw')};
  padding: 0px;
  text-align: center;
  letter-spacing: 1px;
  line-height: 1em;
`;

const PodcastsHeaderContainer = () => {
  const size = React.useContext(ResponsiveContext);
  return (
    <BoxWithBGImage
      url="/talking.webp"
      heigt="medium"
      pad={{ bottom: '1em' }}
      align="center"
      justify="start"
      direction="column">
      <HeadingText
        size={size}
        color={vttOrange}
        margin={size === 'small' ? { bottom: 'none' } : { bottom: '1em' }}>
        PODCASTS AND SHOWS
      </HeadingText>
      <Text
        size="xlarge"
        textAlign="center"
        margin={size === 'small' ? { top: '1em', bottom: '1em' } : { bottom: '2em' }}>
        Telling soldier&apos;s stories
      </Text>
    </BoxWithBGImage>
  );
};

export default PodcastsHeaderContainer;
