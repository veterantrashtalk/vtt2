import React from 'react';
import { render } from '@testing-library/react';
import PodcastsHeaderContainer from './PodcastsHeaderContainer';

describe('PodcastsHeaderContainer tests', () => {
  it('should render without crashing', () => {
    const { container } = render(<PodcastsHeaderContainer />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
