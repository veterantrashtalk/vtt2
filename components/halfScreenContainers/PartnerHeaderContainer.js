import React from 'react';
import BoxWithBGImage from '../shared/BoxWithBGImage';
import { Heading, ResponsiveContext, Text } from 'grommet';
import { vttOrange } from '../../styles/theme';
import styled from 'styled-components';

const HeadingText = styled(Heading)`
  font-weight: 700;
  font-size: ${(props) => (props.size === 'small' ? `60px` : '8vw')};
  padding: 0px;
  text-align: center;
  letter-spacing: 1px;
  line-height: 1em;
`;

const PartnerHeaderContainer = () => {
  const size = React.useContext(ResponsiveContext);

  return (
    <BoxWithBGImage
      url="/partners.webp"
      gradient
      height="medium"
      pad={{ bottom: '1em' }}
      align="center"
      justify="start"
      direction="column">
      <HeadingText
        size={size}
        color={vttOrange}
        margin={size === 'small' ? { bottom: 'none' } : { bottom: '.5em' }}>
        PARTNERS
      </HeadingText>
      <Text size="large" textAlign="center" margin="large">
        Veteran Trash Talk Partners are the companies who have teamed up with us in our mission to
        build a community of veterans to help stop soldier suicide.
      </Text>
    </BoxWithBGImage>
  );
};

export default PartnerHeaderContainer;
