import React from 'react';
import { render } from '@testing-library/react';
import HeaderOneContainer from './HeaderOneContainer';

describe('HeaderOneContainer tests', () => {
  it('should render', () => {
    const { container } = render(<HeaderOneContainer />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
