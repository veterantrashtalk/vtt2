import React from 'react';
import { render } from '@testing-library/react';
import PartnerHeaderContainer from './PartnerHeaderContainer';

describe('PartnerHeaderContainer tests', () => {
  it('should render', () => {
    const { container } = render(<PartnerHeaderContainer />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
