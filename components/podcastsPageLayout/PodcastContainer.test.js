import React from 'react';
import { render } from '@testing-library/react';
import PodcastContainer from './PodcastContainer';

describe('PodcastContainer tests', () => {
  const props = {
    src: '/test',
    title: 'test',
    about: 'tester',
    href: 'test.com'
  };
  it('should render without crashing', () => {
    const { container } = render(<PodcastContainer {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
