import React from 'react';
import { Box, Heading, Text } from 'grommet';
import LinkButton from '../shared/LinkButton';
import Image from 'next/image';
import { LINKS } from 'helpers/globals';
import Award from '@components/shared/Award';

const vttAbout =
  'Veteran Trash Talk Hour podcasts are stories of real warriors dealing with demons on the inside. We are brave enough to share them with our brothers, sisters, supporters, and contributors so we can defeat them together. Our podcasts promote veteran-owned businesses and organizations that support veteran causes. Also, there is plenty of trash talk.';

const AwardWinningPodcast = () => {
  return (
    <Box direction="column" align="center" pad="medium">
      <Box margin="xsmall">
        <Image src={'/vtth.webp'} width={350} height={200} />
      </Box>
      <Heading level="2" textAlign="center" margin={{ top: '1em' }}>
        THE VETERAN TRASH TALK HOUR
      </Heading>
      <Award />
      <Text textAlign="center" size="small" margin={{ bottom: '2em' }}>
        {vttAbout}
      </Text>
      <LinkButton href={LINKS.YOUTUBE} label="Listen Now" />
    </Box>
  );
};
export default AwardWinningPodcast;
