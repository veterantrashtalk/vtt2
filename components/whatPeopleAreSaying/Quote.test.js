import React from 'react';
import { render } from '@testing-library/react';
import Quote from './Quote';

describe('Quote tests', () => {
  const props = {
    name: 'jon',
    text: 'tes'
  };
  it('should render', () => {
    const { container } = render(<Quote {...props} />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
