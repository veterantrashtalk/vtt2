import React from 'react';
import { render } from '@testing-library/react';
import WhatPeopleAreSaying from './WhatPeopleAreSaying';

describe('WhatPeopleAreSaying tests', () => {
  it('should render', () => {
    const { container } = render(<WhatPeopleAreSaying />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
