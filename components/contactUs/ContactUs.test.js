import React from 'react';
import { render } from '@testing-library/react';
import ContactUs from './ContactUs';

describe('ContactUs tests', () => {
  test('should render', () => {
    const { container } = render(<ContactUs />);
    expect(container).not.toBeEmptyDOMElement();
  });
});
