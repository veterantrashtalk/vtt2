import React from 'react';
import { Box, Form, FormField, TextArea, TextInput } from 'grommet';
import useFormHandler from '@components/shared/UseFormHandler';
import FormSubmitButton from '@components/shared/FormSubmitButton';

const ContactForm = () => {
  const emptyInput = { email: '', message: '', name: '' };
  const [contactValue, setContactValue] = React.useState(emptyInput);

  const { status, handleOnSubmit } = useFormHandler(emptyInput, 'send', setContactValue);

  return (
    <Box pad="medium" width="xlarge" align="center" justify="center">
      <Form
        value={contactValue}
        validate="blur"
        onChange={(nextContactValue) => setContactValue(nextContactValue)}
        onSubmit={(e) => handleOnSubmit(e, contactValue)}>
        <FormField name="name" htmlfor="name-input-id" label="name" required>
          <TextInput id="name-input-id" name="name" />
        </FormField>
        <FormField name="email" htmlfor="email-input-id" label="email" required>
          <TextInput id="email-input-id" name="email" type="email" />
        </FormField>
        <FormField
          name="message"
          htmlfor="message-input-id"
          label="message"
          required
          validate={[{ regexp: /^[a-z]/i }]}>
          <TextArea id="message-input-id" name="message" resize={false} />
        </FormField>
        <FormSubmitButton status={status} />
      </Form>
    </Box>
  );
};

export default ContactForm;
